import React from "react";
import Producto from "./components/Producto";
import Nav from "./components/Nav";
import { Provider } from "react-redux";
import generateStore from "./redux/store";
import Carrusel from "./components/HomeCarousel";
import Carrito from "./components/Carrito";
import SimpleReactLightbox from "simple-react-lightbox";

function App() {
  const store = generateStore();

  return (
    <div className="app">
      <SimpleReactLightbox>
        <Provider store={store}>
          <Nav></Nav>
          <div className="desktopDiv">
            <Carrusel></Carrusel>
            <Producto></Producto>
          </div>
        </Provider>
      </SimpleReactLightbox>
    </div>
  );
}

export default App;
