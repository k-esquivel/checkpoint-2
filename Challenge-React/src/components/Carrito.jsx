import React from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { obtenerProductosAccion } from "../redux/prodDucks";
import thumbnail from "./img/image-product-1-thumbnail.jpg";
import borrar from "./svg/icon-delete.svg";
import CarritoImg from "./svg/icon-cart.svg";

const Carrito = () => {
  const dispatch = useDispatch();
  const datosProducto = useSelector((store) => store.producto.array);

  React.useEffect(() => {
    const fetchData = () => {
      dispatch(obtenerProductosAccion());
    };
    fetchData();
  }, [dispatch]);

  const [contador, setContador] = useState(0);
  const [contadorAgregado, setContadorAgregado] = useState(0);
  const [estadoCarrito, setEstadoCarrito] = useState(false);

  const carritoAccion = () => {
    setEstadoCarrito(!estadoCarrito);
  };

  const descuento = datosProducto.discount * 100 + "%";
  const precioConDescuento = datosProducto.discount * datosProducto.price;

  return (
    <div id="agregarProducto">
      <div className="precio">
        <div id="productoDescuento">${precioConDescuento}</div>
        <div id="descuento">{descuento}</div>
      </div>
      <div id="productoPrecio">${datosProducto.price}</div>
      <div className="carritoContador">
        <div className="Carrito-Icono" onClick={carritoAccion}>
          <img src={CarritoImg} alt="" id="Carrito" />
        </div>
        <p className="contadorCarrito">{contadorAgregado}</p>
      </div>
      <div className="contadorAdd">
        <div className="contador">
          {contador < 1 ? (
            <button className="botonContador" disabled>
              -
            </button>
          ) : (
            <button
              className="botonContador"
              onClick={() => {
                setContador(contador - 1);
              }}
            >
              -
            </button>
          )}

          <div>{contador}</div>

          <button
            className="botonContador"
            onClick={() => {
              setContador(contador + 1);
            }}
          >
            +
          </button>
        </div>
        {contador > 0 ? (
          <div className="botonAddCont">
            <button
              className="botonAdd"
              onClick={() => {
                setContadorAgregado(contador);
                setContador(0);
              }}
            >
              <div className="Carrito Icono">
                <img src={CarritoImg} alt="" id="Carrito" />
              </div>
              Add to chart
            </button>
          </div>
        ) : (
          <div className="botonAddCont">
            <button className="botonAdd" id="botonDisabled" disabled>
              <div className="Carrito Icono">
                <img src={CarritoImg} alt="" id="Carrito" />
              </div>
              <div id="add">Add to chart </div>{" "}
            </button>
          </div>
        )}
      </div>

      {estadoCarrito ? (
        <div className="carrito">
          <div className="cart">Cart</div>
          <hr />
          {contadorAgregado > 0 ? (
            <div id="carritoCompra">
              <div id="infoCompra">
                <img src={thumbnail} alt="" id="thumbnailCart" />

                <div className="textoInfo">
                  <div>{datosProducto.name}</div>

                  <div className="textoInfo">
                    ${precioConDescuento} x {contadorAgregado}{" "}
                    <span id="total">
                      {" "}
                      ${precioConDescuento * contadorAgregado}
                    </span>
                  </div>
                </div>

                <button id="borrar">
                  <img
                    id="imgBorrar"
                    src={borrar}
                    alt=""
                    onClick={() => {
                      setContadorAgregado(contadorAgregado - contadorAgregado);
                    }}
                  />
                </button>
              </div>
              <button className="botonAdd" id="checkout">
                Checkout
              </button>
            </div>
          ) : (
            <div id="cartEmpty">Your cart is empty</div>
          )}
        </div>
      ) : null}
    </div>
  );
};

export default Carrito;
