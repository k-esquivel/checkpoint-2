import React, { useState } from "react";
import Carousel from "react-bootstrap/Carousel";
import img1 from "./img/image-product-1.jpg";
import { SRLWrapper } from "simple-react-lightbox";

const data = [
  {
    image: require("./img/image-product-1.jpg"),
  },
  {
    image: require("./img/image-product-2.jpg"),
  },
  {
    image: require("./img/image-product-3.jpg"),
  },
  {
    image: require("./img/image-product-4.jpg"),
  },
];

function HomeCarousel() {
  const [index, setIndex] = useState(0);
  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (
    <div>
      <div className="carousel">
        <Carousel activeIndex={index} onSelect={handleSelect}>
          {data.map((slide, i) => {
            return (
              <Carousel.Item className="carousel">
                <img
                  className="d-block w-100"
                  src={slide.image}
                  alt="slider image"
                />
              </Carousel.Item>
            );
          })}
        </Carousel>
      </div>

      <div className="prodThumbnails">
        <img className="prodPrincipal" src={img1} alt="slider image" />
      </div>

      <SRLWrapper>
        {data.map((slide, i) => {
          return <img className="prodThumbnail" src={slide.image} />;
        })}
      </SRLWrapper>
    </div>
  );
}
export default HomeCarousel;
