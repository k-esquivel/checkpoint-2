import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { obtenerProductosAccion } from "../redux/prodDucks";
import Carrito from "./Carrito";

const Producto = () => {
  const dispatch = useDispatch();
  const producto = useSelector((store) => store.producto.array);

  React.useEffect(() => {
    const fetchData = () => {
      dispatch(obtenerProductosAccion());
    };
    fetchData();
  }, [dispatch]);

  return (
    <div>
      <div className="infoProducto">
        <div id="productoMarca"> {producto.brand}</div>

        <div id="productoNombre">{producto.name}</div>

        <div id="productoDescripcion">{producto.description}</div>
      </div>

      <Carrito></Carrito>
    </div>
  );
};

export default Producto;
