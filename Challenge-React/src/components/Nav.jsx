import React from "react";
import Menu from "./svg/icon-menu.svg";
import Logo from "./svg/logo.svg";
import Cerrar from "./svg/icon-close.svg";
import Perfil from "./img/image-avatar.png";

const Nav = () => {
  const [estadoMenu, setEstadoMenu] = React.useState(false);

  const menuAccion = () => {
    setEstadoMenu(!estadoMenu);
  };

  return (
    <div>
      {estadoMenu ? (
        <nav className="menuMobile">
          <ul>
            <li>Collections</li>
            <li>Men</li>
            <li>Women</li>
            <li>About</li>
            <li>Contact</li>
            <li className="cerrar">
              <img src={Cerrar} alt="" onClick={menuAccion} />
            </li>
          </ul>
        </nav>
      ) : null}
      <header>
        <div className="headerDerecha">
          <div className="menu" onClick={menuAccion}>
            <img src={Menu} alt="" id="menu" />
          </div>

          <div>
            <img src={Logo} alt="" id="Logo" />
          </div>
        </div>

        <nav className="menuDesktop">
          <ul>
            <li>Collections</li>
            <li>Men</li>
            <li>Women</li>
            <li>About</li>
            <li>Contact</li>
          </ul>
          <hr />
        </nav>

        <div className="headerIzq">
          <div className="Perfil">
            <img src={Perfil} alt="" id="Perfil" />
          </div>
        </div>
      </header>
    </div>
  );
};

export default Nav;
