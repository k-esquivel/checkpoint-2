import axios from "axios"


//constantes
const dataInicial = {
array: []
}


//types
const OBTENER_PRODUCTOS_EXITO = 'OBTENER_PRODUCTOS_EXITO'
const OBTENER_PRODUCTO = 'OBTENER_PRODUCTO'



//reducer
export default function prodReducer (state= dataInicial, action){
    switch (action.type) {
        case OBTENER_PRODUCTOS_EXITO:
        return {...state, array: action.payload}
        default:
            return state
    }
}


//acciones
export const obtenerProductosAccion = () => async(dispatch, getState) =>{

dispatch({
    type: OBTENER_PRODUCTO
})

try {
    const res = await axios.get('https://www.mockachino.com/b045b644-d886-4e/products/7d6f7710-95d0-4a27-ae6c-b02c6cb0348f')
    dispatch({
        type: OBTENER_PRODUCTOS_EXITO,
        payload: res.data
    })
} catch (error) {
    console.log(error)
}
}
